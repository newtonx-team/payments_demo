from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.db import transaction

from base.models import BaseModel
from .exceptions import InsufficientFundsError


class Wallet(BaseModel):
    user = models.OneToOneField(
        User,
        related_name='wallet',
        on_delete=models.CASCADE
    )
    balance = models.DecimalField(
        max_digits=12,
        decimal_places=2
    )

    def send_payment(self, to_wallet, amount):
        if not to_wallet:
            raise TypeError('to_wallet is required.')

        if amount <= 0:
            raise ValueError('Amount must be > $0.00')

        if amount > self.balance:
            raise InsufficientFundsError('Insufficient wallet balance.')

        tx = None
        with transaction.atomic():
            tx = Transaction.objects.create(
                from_wallet=self,
                to_wallet=to_wallet,
                amount=amount,
            )

            self.balance -= amount
            self.save()

            to_wallet.balance += amount
            to_wallet.save()

        return tx

    def __str__(self):
        return f'{self.id}: user={self.user}, balance={self.balance}'


class Transaction(BaseModel):
    from_wallet = models.ForeignKey(
        Wallet,
        on_delete=models.CASCADE,
        related_name='sent_transactions'
    )
    to_wallet = models.ForeignKey(
        Wallet,
        on_delete=models.CASCADE,
        related_name='received_transactions'
    )
    amount = models.DecimalField(
        max_digits=10,
        decimal_places=2
    )

    def save(self, **kwargs):
        if self.from_wallet == self.to_wallet:
            raise ValidationError(
                'source and destination wallets must be different.'
            )

        return super().save(**kwargs)
