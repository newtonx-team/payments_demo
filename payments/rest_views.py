from django.core.exceptions import (
    ValidationError as CoreValidationError
)
from rest_framework.exceptions import ValidationError
from rest_framework.views import APIView
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView
)
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_201_CREATED
)

from .exceptions import InsufficientFundsError
from .models import (
    Wallet,
    Transaction
)
from .permissions import (
    WalletPermission
)
from .serializers import (
    MyWalletSerializer,
    WalletSerializer,
    SentTransactionSerializer,
    ReceivedTransactionSerializer,
    SendPaymentSerializer
)


class UserWalletRetrieveAPIView(RetrieveAPIView):
    """
    Returns the current user's Wallet.
    """
    serializer_class = MyWalletSerializer

    def get_object(self):
        return Wallet.objects.get(user=self.request.user)


class WalletRetrieveAPIView(RetrieveAPIView):
    """
    Returns a Wallet.
    """
    queryset = Wallet.objects.all()
    serializer_class = WalletSerializer
    permission_classes = (WalletPermission,)


class UserSentTransactionsListAPIView(ListAPIView):
    """
    Returns an array of Transactions sent by the current user.
    """
    serializer_class = SentTransactionSerializer

    def get_queryset(self):
        return Transaction.objects.filter(from_wallet=self.request.user.wallet)


class UserReceivedTransactionsListAPIView(ListAPIView):
    """
    Returns an array of Transactions received by the current user.
    """
    serializer_class = ReceivedTransactionSerializer

    def get_queryset(self):
        return Transaction.objects.filter(to_wallet=self.request.user.wallet)


class SendPaymentAPIView(APIView):
    """
    Transfers an amount of funds from one Wallet to another.
    """

    def post(self, request, format=None):
        serializer1 = SendPaymentSerializer(data=request.data)
        serializer1.is_valid(raise_exception=True)

        data = serializer1.validated_data
        to_wallet_id = data['to_wallet']
        amount = data['amount']

        try:
            to_wallet = Wallet.objects.get(id=to_wallet_id)
        except Wallet.DoesNotExist:
            raise ValidationError({
                'error': "'to_wallet' does not exist."
            })

        wallet = Wallet.objects.get(user=self.request.user)

        try:
            tx = wallet.send_payment(to_wallet, amount)
        except (InsufficientFundsError, CoreValidationError) as e:
            raise ValidationError({
                'error': e
            })

        serializer2 = SentTransactionSerializer(tx)
        sent_tx = serializer2.data
        return Response(data=sent_tx, status=HTTP_201_CREATED)
