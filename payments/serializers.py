from django.contrib.auth.models import User

from rest_framework import serializers

from .models import (
    Wallet,
    Transaction
)


class WalletUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name')
        read_only_fields = ('id', 'username', 'first_name', 'last_name')


class MyWalletSerializer(serializers.ModelSerializer):
    user = WalletUserSerializer()

    class Meta:
        model = Wallet
        fields = ('id', 'user', 'balance', 'created_at', 'updated_at')
        read_only_fields = ('id', 'user', 'created_at', 'updated_at')


class WalletSerializer(serializers.ModelSerializer):
    user = WalletUserSerializer()

    class Meta:
        model = Wallet
        fields = ('id', 'user', 'created_at', 'updated_at')
        read_only_fields = ('id', 'user', 'created_at', 'updated_at')


class TransactionWalletSerializer(serializers.ModelSerializer):
    user = WalletUserSerializer()

    class Meta:
        model = Wallet
        fields = ('id', 'user')
        read_only_fields = ('id', 'user')


class SentTransactionSerializer(serializers.ModelSerializer):
    to_wallet = TransactionWalletSerializer()

    class Meta:
        model = Transaction
        fields = (
            'id', 'to_wallet', 'amount', 'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id', 'to_wallet', 'amount', 'created_at',
            'updated_at'
        )


class ReceivedTransactionSerializer(serializers.ModelSerializer):
    from_wallet = TransactionWalletSerializer()

    class Meta:
        model = Transaction
        fields = (
            'id', 'from_wallet', 'amount', 'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id', 'from_wallet', 'amount', 'created_at',
            'updated_at'
        )


class SendPaymentSerializer(serializers.Serializer):
    to_wallet = serializers.UUIDField()
    amount = serializers.DecimalField(
        max_digits=10,
        decimal_places=2,
        min_value=0.01
    )
