from django.urls import path

from payments.rest_views import (
    UserWalletRetrieveAPIView,
    WalletRetrieveAPIView,
    UserSentTransactionsListAPIView,
    UserReceivedTransactionsListAPIView,
    SendPaymentAPIView
)

urlpatterns = [
    path(
        'wallet/',
        UserWalletRetrieveAPIView.as_view(),
        name='my_wallet_detail'
    ),
    path(
        'wallets/<pk>/',
        WalletRetrieveAPIView.as_view(),
        name='wallet_detail'
    ),
    path(
        'sent_transactions/',
        UserSentTransactionsListAPIView.as_view(),
        name='sent_transactions_list'
    ),
    path(
        'received_transactions/',
        UserReceivedTransactionsListAPIView.as_view(),
        name='received_transactions_list'
    ),
    path(
        'send_payment/',
        SendPaymentAPIView.as_view(),
        name='send_payment'
    )
]
