from django.contrib import admin
from django.urls import path, include

from rest_framework import permissions
from rest_framework_jwt.views import obtain_jwt_token

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from api import urls as api_urls

schema_view = get_schema_view(
    openapi.Info(
        title="Payments Demo API",
        default_version='v1',
        description='Simple payments API',
        terms_of_service=None,
        contact=None,
        license=None
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path(
        'swagger(<format>\.json|\.yaml)/',
        schema_view.without_ui(cache_timeout=0),
        name='schema-json'
    ),
    path(
        'swagger/',
        schema_view.with_ui('swagger', cache_timeout=0),
        name='schema-swagger-ui'
    ),
    path(
        'redoc/',
        schema_view.with_ui('redoc', cache_timeout=0),
        name='schema-redoc'
    ),
    path('admin/', admin.site.urls),
    path('api/', include(api_urls)),
    path('api-token-auth/', obtain_jwt_token),
]
