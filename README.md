# payments_demo

Payments backend API built with [Django](https://www.djangoproject.com/).

## Usage

If [Docker](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/) are installed:

```bash
docker-compose up
```

Thats it! The application will run on [port 8000](http://localhost:8000).

## Swagger Documentation

API details can be found [here](http://localhost:8000/swagger/).

Note: The JWT should sent in the following format

`Authorization: jwt <token>`

## Requirements

Use this REST API to build a frontend SPA (Angular or React) 
which satisfies the details of [this document](https://docs.google.com/document/d/1e8kun6KSYqK7wu9BMzUra7QFTIyuCGmCaH7p5iL5zDQ).
