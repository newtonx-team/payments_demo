from django.contrib.auth.models import User

from rest_framework import serializers

from payments.models import Wallet


class WalletSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wallet
        fields = ('id', 'created_at', 'updated_at')
        ref_name = 'social.WalletSerializer'


class UserSerializer(serializers.ModelSerializer):
    wallet = WalletSerializer()

    class Meta:
        model = User
        fields = (
            'id', 'username', 'first_name', 'last_name', 'date_joined',
            'last_login', 'wallet'
        )
        read_only_fields = (
            'id', 'username', 'date_joined', 'last_login', 'wallet'
        )


class MyWalletSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wallet
        fields = ('id', 'balance', 'created_at', 'updated_at')
        ref_name = 'social.MyWalletSerializer'


class MyUserSerializer(serializers.ModelSerializer):
    wallet = MyWalletSerializer()

    class Meta:
        model = User
        fields = (
            'id', 'username', 'first_name', 'last_name', 'date_joined',
            'last_login', 'wallet'
        )
        read_only_fields = (
            'id', 'username', 'date_joined', 'last_login', 'wallet'
        )
