from django.urls import path

from .rest_views import (
    UserListAPIView,
    UserRetrieveAPIView,
    MyUserAPIView
)

urlpatterns = [
    path(
        'users/',
        UserListAPIView.as_view(),
        name='users_list'
    ),
    path(
        'users/<pk>/',
        UserRetrieveAPIView.as_view(),
        name='user_detail'
    ),
    path(
        'user/',
        MyUserAPIView.as_view(),
        name='my_user_detail'
    )
]
