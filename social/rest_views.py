from django.contrib.auth.models import User

from rest_framework.filters import SearchFilter
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView
)

from .serializers import (
    UserSerializer,
    MyUserSerializer
)


class UserListAPIView(ListAPIView):
    """
    Returns an array of Users that match the search terms.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (SearchFilter,)
    search_fields = ('username', 'first_name', 'last_name')


class UserRetrieveAPIView(RetrieveAPIView):
    """
    Returns a User.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class MyUserAPIView(RetrieveAPIView):
    """
    Returns the current authenticated User.
    """
    serializer_class = MyUserSerializer

    def get_object(self):
        return User.objects.get(id=self.request.user.id)
