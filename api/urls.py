from django.conf.urls import include

from api.v1 import urls as v1_urls

from django.urls import path

urlpatterns = [
    path('v1/', include(v1_urls)),
]
