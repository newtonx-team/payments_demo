from django.urls import path
from django.conf.urls import include

from payments import urls as payments_urls
from social import urls as social_urls

urlpatterns = [
    path('payments/', include(payments_urls)),
    path('social/', include(social_urls))
]
